//
//  ABSurfaceView.h
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABShapeView.h"

@protocol ABSurfaceViewDelegate <NSObject>

-(void)updatePositionDelta:(CGPoint)posDelta forShape:(ABShapeView *)shapeView;
-(void)pinchDetected:(UIPinchGestureRecognizer*)recognizer;
-(void)panDetected:(UIPanGestureRecognizer*)recognizer;
-(void)longPressDetected:(UILongPressGestureRecognizer*)recognizer;

@end

@interface ABSurfaceView : UIView <ABShapeViewDelegate>

@property NSMutableSet *shapeSet;
@property id<ABSurfaceViewDelegate> delegate;
-(void)addShape:(ABShapeView *)shape;
-(void)removeShape:(ABShapeView *)shape;
-(void)checkForCollisions;

@end
