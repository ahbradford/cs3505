//
//  ABShapeView.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABShapeView.h"
#import <QuartzCore/QuartzCore.h>

@interface ABShapeView()

@property UIPinchGestureRecognizer *pinch;
@property UIPanGestureRecognizer *pan;
@property UILongPressGestureRecognizer *longPress;

@end

@implementation ABShapeView

- (id)initWithFrame:(CGRect)frame type:(NSInteger)type
{
    self = [super initWithFrame:frame];
    if (self)
	{
        _type = type;
		self.backgroundColor = [UIColor clearColor];
        
        //create gesture recgonizers
		_pinch = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchDetected:)];
		_longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressDetected:)];
		_pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panDetected:)];
        
        //add getsture recognizers to self
		[self addGestureRecognizer:_pinch];
		[self addGestureRecognizer:_pan];
		[self addGestureRecognizer:_longPress];
        
        //set delegates of gesture recognizers
		_pan.delegate = self;
		_longPress.delegate = self;
		_pinch.delegate = self;
        
        //set up self for multiple gesture recoginizers
		self.userInteractionEnabled = YES;
		self.multipleTouchEnabled = YES;
		self.contentMode = UIViewContentModeRedraw;
		
		
    }
    return self;
}

//draws based on the current _type (number of sides)
- (void)drawRect:(CGRect)rect
{
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGRect currentRect = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width,self.bounds.size.height);
	[_currentColor setFill];
	[[UIColor blackColor]setStroke];
	CGContextSetLineWidth(ctx, 1);
    
    //Because of the collisions being determined by the frame, we need the shapes to completely fill the frame, so we have to draw the hard way.
    switch (_type)
    {
        case 2:
            
            CGContextAddEllipseInRect(ctx, currentRect);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
            
        case 3:
            
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x+self.bounds.size.width, self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
            
        case 4:
            
            CGContextAddRect(ctx, currentRect);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
            
        case 5:
        {
            float thirdY = self.bounds.origin.y + self.bounds.size.height/2.7;
            float fifthX = self.bounds.origin.x + self.bounds.size.width/5;
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width , thirdY);
            CGContextAddLineToPoint(ctx, fifthX*4, self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, fifthX, self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x , thirdY);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
        }
            
        case 6:
        {
        
            float firstQuarterX = self.bounds.origin.x + self.bounds.size.width*.26;
            float thirdQuarterX = self.bounds.origin.x + self.bounds.size.width*.74;
            float halfY = self.bounds.origin.y + self.bounds.size.height/2;
            CGContextMoveToPoint(ctx, firstQuarterX, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, thirdQuarterX , self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width,halfY);
            CGContextAddLineToPoint(ctx, thirdQuarterX,halfY*2);
            CGContextAddLineToPoint(ctx, firstQuarterX,halfY*2);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x,halfY);
            CGContextAddLineToPoint(ctx, firstQuarterX, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
        }
        
        case 7:
        {
            
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.9 , self.bounds.origin.y+self.bounds.size.height*1/6);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width, self.bounds.origin.y+self.bounds.size.height*3/5);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*6/8 , self.bounds.origin.y+self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*2/8, self.bounds.origin.y+self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y+self.bounds.size.height*3/5);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.1, self.bounds.origin.y+self.bounds.size.height*1/6);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
        }
            
        case 8:
        {
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.3, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.7 , self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width, self.bounds.origin.y+self.bounds.size.height*.3);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width, self.bounds.origin.y+self.bounds.size.height*.7);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*.7, self.bounds.origin.y+self.bounds.size.height);
             CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*.3, self.bounds.origin.y+self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y+self.bounds.size.height*.7);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y+self.bounds.size.height*.3);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.3, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
        }
        case 9:
        {
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.82 , self.bounds.origin.y + self.bounds.size.height*.13);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width, self.bounds.origin.y+self.bounds.size.height*.43);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.93, self.bounds.origin.y+self.bounds.size.height*.77);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*.66, self.bounds.origin.y+self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*.33, self.bounds.origin.y+self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x +self.bounds.size.width*.06, self.bounds.origin.y+self.bounds.size.height*.77);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y+self.bounds.size.height*.43);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.18 , self.bounds.origin.y+ self.bounds.size.height*.13);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width/2, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
        }
            
        case 10:
            CGContextMoveToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.33, self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.66 , self.bounds.origin.y);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.9 , self.bounds.origin.y + self.bounds.size.height*.2);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width , self.bounds.origin.y + self.bounds.size.height*.5);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.9 , self.bounds.origin.y + self.bounds.size.height*.8);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.66 , self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.33 , self.bounds.origin.y + self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.1 , self.bounds.origin.y + self.bounds.size.height*.8);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x, self.bounds.origin.y + self.bounds.size.height*.5);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.1 , self.bounds.origin.y + self.bounds.size.height*.2);
            CGContextAddLineToPoint(ctx, self.bounds.origin.x + self.bounds.size.width*.33, self.bounds.origin.y);
            CGContextDrawPath(ctx, kCGPathFillStroke);
            return;
            
        default:
            break;
    }
}

#pragma mark UIGestureRecognizer methods
-(void)pinchDetected:(UIPinchGestureRecognizer*)recognizer
{
	[_delegate pinchDetected:recognizer];
}

-(void)panDetected:(UIPanGestureRecognizer*)recognizer
{
	[_delegate panDetected:recognizer];
}

-(void)longPressDetected:(UILongPressGestureRecognizer*)recognizer
{
	[_delegate longPressDetected:recognizer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark NSCopyingMethods
- (id)copyWithZone:(NSZone *)zone
{
    ABShapeView *shapeView = [[ABShapeView alloc]init];
    shapeView.currentColor = _currentColor;
    shapeView.frame = self.frame;
    shapeView.positionDelta = _positionDelta;
    return shapeView;
}

#pragma mark ABShapeView methods
- (UIImage *)imageByRenderingView
{
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

@end
