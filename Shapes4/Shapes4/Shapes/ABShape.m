//
//  ABShape.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABShape.h"

@implementation ABShape



#pragma mark NSCopying protocol methods
- (id)copyWithZone:(NSZone *)zone
{
    ABShape *shape = [[ABShape alloc]init];
    shape.color = _color;
    shape.frame = _frame;
    shape.positionDelta = _positionDelta;
    shape.type = _type;
    return shape;
}

#pragma mark NSCoding protocol methods
-(id)initWithCoder:(NSCoder *)decoder
{
	if ((self=[super init])) {
        _frame = [decoder decodeCGRectForKey:@"frame"];
        float x = [decoder decodeDoubleForKey:@"positionDeltax"];
		float y = [decoder decodeDoubleForKey:@"positionDeltay"];
		_color = [decoder decodeObjectForKey:@"color"];
		_type = [decoder decodeIntegerForKey:@"type"];
        _positionDelta = CGPointMake(x, y);
	
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    
    if(_positionDelta.x < .0001 && _positionDelta.x > -.0001)
    {
        
    }
	[encoder encodeCGRect:_frame forKey:@"frame"];
	[encoder encodeDouble:(double)_positionDelta.x forKey:@"positionDeltax"];
	[encoder encodeDouble:(double)_positionDelta.y forKey:@"positionDeltay"];
	[encoder encodeObject:_color forKey:@"color"];
	[encoder encodeInteger:_type forKey:@"type"];
}


@end
