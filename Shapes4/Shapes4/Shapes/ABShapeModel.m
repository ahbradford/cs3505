//
//  ABShapeModel.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABShapeModel.h"
#import "ABShape.h"


#define REST_URL (@"http://eng.utah.edu/~cs4962/shapes/index_slow.php/ahbradfo/")




@interface ABShapeModel()

@property (atomic,strong) NSMutableArray *shapesArray;
@property NSInteger numOfShapes;
@property NSOperationQueue *requestQueue;
@property NSDate *mostRecentUpdate;
@property NSDate *currentTime;
@property NSInteger initialShapeCounter;

@end


@implementation ABShapeModel

@synthesize delegate = _delegate;


-(id)init
{
    self = [super init];
    if(self)
    {
    //Cant really see using more than 30 objects...
    _shapesArray = [[NSMutableArray alloc]initWithCapacity:30];
    
    _requestQueue = [[NSOperationQueue alloc]init];
        _currentTime = [NSDate date];
      
        [self getNumberofShapesFromWebServiceAndInitalize];
    
        
      
        
        
        
    }
    return self;    
}


#pragma mark update model Methods

-(void)loadModelFromWebService
{
    //get all shapes from web service
    for(int i = 0; i < _numOfShapes; i ++)
    {
        [_shapesArray addObject: [self getShapeFromWebServiceAtIndex:i]];
    }
    
    for(ABShape * shape in _shapesArray)
    {
        [_delegate shapeAdded:shape];
    }
}

//updates position delta for a given shape
-(void)updateShapePositionDelta:(CGPoint)posDelta forShape:(ABShape *)shape;
{
    
    
}

//update shapeFrame for a given shape
-(void)updateShapeFrame:(CGRect)frame forShape:(ABShape *)shape;
{
    shape.frame = frame;
	[_delegate shapeWillChange:shape];
    [self updateShapeFromWebServiceAtIndex:[_shapesArray indexOfObject:shape]];
    //[_delegate shapeDidChange:shape];
}

//update shapeColor for a given shape
-(void)updateShapeColor:(UIColor *)color forShape:(ABShape *)shape;
{
	[_delegate shapeWillChange:shape];
    shape.color = color;;
    [self updateShapeFromWebServiceAtIndex:[_shapesArray indexOfObject:shape]];
    //[_delegate shapeDidChange:shape];
}

//adds a shape to the model and notifies delegate
-(void)addShape:(ABShape *)shape
{
    [self addShapeToWeb:shape];
   // [_shapesArray addObject:shape];
    
}

//removes a shape from the model and notifies the delegate.
-(void)removeShape:(ABShape *)shape
{
    
    [self deleteShapeFromWebService:[_shapesArray indexOfObject:shape]];
    
}

//fire this at a rate of .015s, or ELSE!!!!
-(void)updateModelLogic
{
    
//    for(ABShape* s in _shapesArray)
//    {
//        //calculate new frame using position delta
//        CGRect newFrame;
//        newFrame.origin.x = s.frame.origin.x + s.positionDelta.x;
//        newFrame.origin.y = s.frame.origin.y + s.positionDelta.y;
//        newFrame.size = s.frame.size;
//        s.frame = newFrame;
//        
//        //normalize position delta to slow things down to a normal rate
//        CGPoint point = s.positionDelta;
//        if(point.x > .001) point.x = point.x - .0001;
//        if(point.y > .001) point.y = point.y - .0001f;
//        if(point.x < -.001) point.x = point.x + .0001f;
//        if(point.y < -.001) point.y = point.y + .0001f;
//       
//        s.positionDelta = point;
//        
//        
//        //notifiy delegate
//        [_delegate shapeDidChange:s];
//    
//        
//    }
}

//returns array of all ABshapes
-(NSArray *)getAllShapes
{
    NSMutableArray *newArray = [NSMutableArray array];
    for(ABShape *s in _shapesArray)
    {
        [newArray addObject:[s copy]];
    }
    return newArray;
}


#pragma mark NSCoder protocol methods
-(id)initWithCoder:(NSCoder *)decoder
{
	if ((self=[super init])) {
        _shapesArray = [decoder decodeObjectForKey:@"shapesArray"];
		
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:_shapesArray forKey:@"shapesArray"];
	
}

-(void)getNumberofShapesFromWebServiceAndInitalize
{

	_initialShapeCounter = 0;
    
	NSURL *url = [NSURL URLWithString:REST_URL];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];

    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest:request
                                      queue:_requestQueue
                          completionHandler:^(NSURLResponse *response,
                                                         NSData *data,
                                                       NSError *error)
    {
        
        NSDictionary* responseDictionary = [NSPropertyListSerialization propertyListWithData:data
                                                                                     options:0
                                                                                      format:NULL error:NULL];
        
        NSString *timeStamp = [responseDictionary objectForKey:@"TimeStamp"];
        NSDateFormatter *dateFormatter =  [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        _mostRecentUpdate = [dateFormatter dateFromString:timeStamp];
        
        
        
        
        NSString  *number = [NSString stringWithFormat: @"%@",[responseDictionary objectForKey:@"Shapes"]];
        
    
       
        NSArray *bits = [number componentsSeparatedByString: @" "];
        number = bits[0];
        NSInteger numOfItems = number.integerValue;
        
        //send delegate response on the main thread
        
        for(int i = 0; i<numOfItems; i++) [_shapesArray addObject: [NSNull null]];
        
        for(int i = 0; i < numOfItems;i++)
        {
            [_requestQueue addOperationWithBlock: ^
            {
                ABShape *shape = [self getShapeFromWebServiceAtIndex:i];
                [[NSOperationQueue mainQueue]addOperationWithBlock:^
                           {
                               _shapesArray[i] = shape;
							   _initialShapeCounter++;
                               if(_initialShapeCounter == numOfItems)
							   {
								   [self notifyDelegateofAllShapes];
								   
							   }
                           }];
            }];
        }
        
        
    }];
   
}

-(void)notifyDelegateofAllShapes
{
	for (ABShape *s in _shapesArray)
	{
		[_delegate shapeAdded:s];
	}
}

//We can send this one synchronous because it is only called from within the getAllShapesFromWebServiceBlock,
//Already on a different Queue.
-(ABShape *)getShapeFromWebServiceAtIndex:(NSInteger)num
{
    NSURL *url = [NSURL URLWithString:[REST_URL stringByAppendingString:[NSString stringWithFormat:@"%d/",num]]];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:NULL error:NULL];
    
    return [self dataToShape:responseData];
}

-(void)updateShapeFromWebServiceAtIndex:(NSInteger)num
{
    NSData *data = [self shapeToData:_shapesArray[num]];
    
    NSURL *url = [NSURL URLWithString:[REST_URL stringByAppendingString:[NSString stringWithFormat:@"%d/",num]]];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:_requestQueue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error)
     {
         
         NSDictionary* responseDictionary = [NSPropertyListSerialization propertyListWithData:data
                                                                                      options:0
                                                                                       format:NULL error:NULL];
         
         NSInteger numOfItems;
         id number =  [responseDictionary objectForKey:@"Shape"];
         
         if([number isKindOfClass:[NSString class]])
         {
             NSString *num = number;
             
             numOfItems =  num.integerValue;
         }
         if([number isKindOfClass:[NSNumber class]])
         {
             NSNumber *num = number;
             numOfItems =  num.integerValue;
         }
         
         //send delegate response on the main thread
         
         
        ABShape *newShape = [self getShapeFromWebServiceAtIndex:num];
		 ABShape *shape = [_shapesArray objectAtIndex:num];
		 
		 shape.frame = newShape.frame;
		 shape.color = newShape.color;
		 shape.type = newShape.type;
             
         [[NSOperationQueue mainQueue]addOperationWithBlock:^
        {
                [_delegate shapeDidChange:shape];
        }];
         
         
         
     }];
}

-(void)deleteShapeFromWebService:(NSInteger)num
{
    NSURL *url = [NSURL URLWithString:[REST_URL stringByAppendingString:[NSString stringWithFormat:@"%d/",num]]];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"DELETE"];
    
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue: _requestQueue
     completionHandler:^(NSURLResponse *response,
                                NSData *data,
                                NSError *error)
    {
    
        NSDictionary* responseDictionary = [NSPropertyListSerialization propertyListWithData:data
                                                                        options:0
                                                                        format:NULL error:NULL];
        
        //this gets the index of the shape that was deleted.  It is in the format "Shape 1 Deleted"
        NSString *responseString = [responseDictionary objectForKey:@"Shapes"];
        NSArray *bits = [responseString componentsSeparatedByString: @" "];
        NSString* number = bits[0];
        NSInteger index = number.integerValue;
        
        
        ABShape *shape = [_shapesArray objectAtIndex:index];
        [_shapesArray removeObjectAtIndex:[_shapesArray indexOfObject:shape]];
        
        //send delegate response on the main thread
        [[NSOperationQueue mainQueue]addOperationWithBlock:^
        {
            [_delegate shapeRemoved:shape];
        }];
                       
    }];
    
    
}

-(void)addShapeToWeb:(ABShape *)shape
{
    
    NSData *data = [self shapeToData:shape];
    
    NSURL *url = [NSURL URLWithString:REST_URL];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:_requestQueue completionHandler:^(NSURLResponse *response,
                                                                                            NSData *data,
                                                                                            NSError *error)
    {
        
        NSDictionary* responseDictionary = [NSPropertyListSerialization propertyListWithData:data
                                                                                     options:0
                                                                                      format:NULL error:NULL];
        
        //this gets the index of the shape that was added.  
        NSNumber * number = [responseDictionary objectForKey:@"Shape"];
        NSInteger index = number.integerValue;
        
        ABShape *shape = [self getShapeFromWebServiceAtIndex:index];
        [_shapesArray  insertObject:shape atIndex:index];
        
        
        //send delegate response on the main thread
        [[NSOperationQueue mainQueue]addOperationWithBlock:^
         {
             
             [_delegate shapeAdded:shape];
         }];
        
    }];
    
    
}

-(ABShape *)dataToShape:(NSData *)data
{
    NSDictionary* responseDictionary = [NSPropertyListSerialization propertyListWithData:data options:0 format:NULL error:NULL];
    
    NSArray *array = [responseDictionary objectForKey:@"Shapes"];
    responseDictionary = array[0];
    
    //get values from returned web shape
    NSNumber *sideCount = [responseDictionary objectForKey:@"SideCount"];
    NSNumber *width = [responseDictionary objectForKey:@"SizeWidth"];
    NSNumber *height = [responseDictionary objectForKey:@"SizeHeight"];
    NSNumber *centerX = [responseDictionary objectForKey:@"CenterX"];
    NSNumber *centerY = [responseDictionary objectForKey:@"CenterY"];
    NSNumber *red = [responseDictionary objectForKey:@"ColorRed"];
    NSNumber *green = [responseDictionary objectForKey:@"ColorGreen"];
    NSNumber *blue = [responseDictionary objectForKey:@"ColorBlue"];
    NSNumber *alpha = [responseDictionary objectForKey:@"ColorAlpha"];
    
    //create shape and add.
    ABShape *shape =  [[ABShape alloc]init];
    
    shape.type = sideCount.integerValue;
    
    shape.frame = CGRectMake(centerX.floatValue - width.floatValue/2,
                             centerY.floatValue - height.floatValue/2,
                             width.floatValue,
                             height.floatValue);
    shape.color = [[UIColor alloc]initWithRed:red.floatValue
                                        green:green.floatValue
                                         blue:blue.floatValue
                                        alpha:alpha.floatValue];
    
    return shape;

}

-(NSData *)shapeToData:(ABShape *)shape
{
    //create dictionary
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    //add frame
    [dictionary setObject:@(shape.frame.origin.x + shape.frame.size.width/2) forKey:@"CenterX"];
    [dictionary setObject:@(shape.frame.origin.y + shape.frame.size.height/2) forKey:@"CenterY"];
    [dictionary setObject:@(shape.frame.size.height) forKey:@"SizeHeight"];
    [dictionary setObject:@(shape.frame.size.width) forKey:@"SizeWidth"];
    
    //addColors
    const float* colors = CGColorGetComponents( shape.color.CGColor );
    [dictionary setObject:@(colors[0]) forKey:@"ColorRed"];
    [dictionary setObject:@(colors[1]) forKey:@"ColorGreen"];
    [dictionary setObject:@(colors[2]) forKey:@"ColorBlue"];
    [dictionary setObject:@(colors[3]) forKey:@"ColorAlpha"];
    
    //add side count
    [dictionary setObject:@(shape.type) forKey:@"SideCount"];
    
    //make data
    NSData *data = [NSPropertyListSerialization dataFromPropertyList:dictionary format:NSPropertyListXMLFormat_v1_0 errorDescription:nil];
    
    
    return data;
}

@end


