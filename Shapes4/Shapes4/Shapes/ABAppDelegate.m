//
//  ABAppDelegate.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABAppDelegate.h"

#import "ABMasterViewController.h"

#import "ABDetailViewController.h"
#import "ABShapeModel.h"

@interface ABAppDelegate()

@property ABShapeModel* model;
@property ABDetailViewController* detailViewController;
@property ABMasterViewController* masterViewController;
@property NSTimer * timer;

@end

@implementation ABAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *savedData = [documentsDirectory stringByAppendingPathComponent:@"savedData.ABS"];
    
    _model = [[ABShapeModel alloc]init];
    
    
    //set up delegates
    _model.delegate = self;
	_masterViewController = [[ABMasterViewController alloc] initWithNibName:@"ABMasterViewController" bundle:nil];
	_detailViewController = [[ABDetailViewController alloc] initWithNibName:@"ABDetailViewController" bundle:nil];
    
    _detailViewController.masterViewController = _masterViewController;
    
    //add models to ViewControllers
    _masterViewController.model = _model;
    _detailViewController.model = _model;
    
    //split view setup.
    UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:_masterViewController];
	UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:_detailViewController];
	_masterViewController.detailViewController = _detailViewController;
    

	self.splitViewController = [[UISplitViewController alloc] init];
	self.splitViewController.delegate = _detailViewController;
	self.splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];
	self.splitViewController.presentsWithGesture = NO;
	self.window.rootViewController = self.splitViewController;
    [self.window makeKeyAndVisible];
    
    
    [_model loadModelFromWebService];
   
    
    
//	//Load objects from savedData into model.
//    if([[NSFileManager defaultManager]fileExistsAtPath:savedData])
//	{
//		NSArray *savedDataArray = [NSKeyedUnarchiver unarchiveObjectWithFile:savedData];
//		for(ABShape *s in savedDataArray)
//        {
//         //   [_model addShape:s];
//        }
//	}
    return YES;
}

#pragma mark ABShapeModelDelegate Methods

-(void)shapeDidChange:(ABShape *)shape
{
    [_detailViewController updateShape:shape];
    [_masterViewController updateShape:shape];
}

-(void)shapeAdded:(ABShape *)shape
{
    [_detailViewController addShape:shape];
    [_masterViewController addShape:shape];
}

-(void)shapeRemoved:(ABShape *)shape
{
    [_detailViewController removeShape:shape];
    [_masterViewController removeShape:shape];
}

-(void)shapeWillChange:(ABShape *)shape
{
	[_masterViewController shapeWillChange:shape];
}

//restarts timer if application reenters foreground
-(void)applicationWillEnterForeground:(UIApplication *)application
{
	if(_timer)[_timer invalidate];
	_timer = [NSTimer scheduledTimerWithTimeInterval:.015 target:_model selector:@selector(updateModelLogic) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

//Stops timer and saves model to savedData file
-(void)applicationWillResignActive:(UIApplication *)application
{
	//[_timer invalidate];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *savedData = [documentsDirectory stringByAppendingPathComponent:@"savedData.ABS"];
	[NSKeyedArchiver archiveRootObject:[_model getAllShapes] toFile:savedData];
}

//restarts timer if application becomes active
-(void)applicationDidBecomeActive:(UIApplication *)application
{
//Timer is only for local storage with animations.
//	if(_timer)[_timer invalidate];
//    _timer = [NSTimer scheduledTimerWithTimeInterval:.015 target:_model selector:@selector(updateModelLogic) userInfo:nil repeats:YES];
//    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}


@end
