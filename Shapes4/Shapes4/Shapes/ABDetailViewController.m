//
//  ABDetailViewController.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABDetailViewController.h"
#import "ABSurfaceView.h"
#import "ABShape.h"
#import "ABShapeModel.h"
#import "ABColorPicker.h"
#import "ABMasterViewController.h"

@interface ABDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) UIPopoverController *colorPickerPopoverController;
@property (strong, nonatomic)  UIViewController *colorPickerViewController;
@property (strong, nonatomic) ABColorPicker *colorPicker;
@property (nonatomic,weak) ABShapeView* currentShapeSelectedbyColorPicker;
@property (strong,nonatomic) NSMutableDictionary *shapesToViews;
@property (strong,nonatomic) NSMutableDictionary *viewsToShapes;

- (void)configureView;

@end

@implementation ABDetailViewController

@synthesize colorPickerPopoverController = _colorPickerPopoverController;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

	if (self.detailItem)
    {
		 self.detailDescriptionLabel.text = [self.detailItem description];
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
		self.title = NSLocalizedString(@"Shapes", @"Shapes");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[self configureView];
    UIInterfaceOrientation orientation = self.interfaceOrientation;
    
    //hides the navagation bar when in landscape
	if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
	{
		self.navigationController.navigationBarHidden = YES;
	}
	
    _viewsToShapes = [[NSMutableDictionary alloc]init];
    _shapesToViews = [[NSMutableDictionary alloc]init];
    _surfaceView.delegate = self;
    
    //load up shapes from the model
    NSArray *shapes = [_model getAllShapes];
    for(ABShape* s in shapes)
    {
        [self addShape:s];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark add,remove, and update shapes
-(void)addShape:(ABShape *)shape
{
    //adds shapes to the dictionarys and to the surfaceView
    ABShapeView *shapeView = [[ABShapeView alloc]initWithFrame:[self convertFrameFromModel:shape.frame] type:shape.type];
    shapeView.positionDelta = shape.positionDelta;
    shapeView.currentColor = shape.color;
    [_surfaceView addShape:shapeView];
    [_shapesToViews setObject:shapeView forKey:[NSValue valueWithPointer:CFBridgingRetain(shape)]];
    [_viewsToShapes setObject:shape forKey:[NSValue valueWithPointer:CFBridgingRetain(shapeView)]];
}

//update shape from model change
-(void)updateShape:(ABShape *)shape
{
    ABShapeView * shapeView = _shapesToViews[[NSValue valueWithPointer: (__bridge const void *)(shape)]];
	
	//If we are currently changing the color of the object, it should not move.
	//Update the model to the previous location
	if(shapeView == _currentShapeSelectedbyColorPicker)
	{
		shapeView.currentColor = shape.color;
		[shapeView setNeedsDisplay];
		return;
	}
	
    shapeView.frame = [self convertFrameFromModel:shape.frame];
    shapeView.positionDelta = [self convertPosDeltaFromModel: shape.positionDelta];
    shapeView.currentColor = shape.color;
    [_surfaceView checkForCollisions];
    [shapeView setNeedsDisplay];
}

//converts frame from model to views cocrdinates
-(CGRect)convertFrameFromModel:(CGRect)frame
{
    return CGRectMake(frame.origin.x * _surfaceView.frame.size.width,
                      frame.origin.y * _surfaceView.frame.size.height,
                      frame.size.width * _surfaceView.frame.size.width,
                      frame.size.height * _surfaceView.frame.size.height);
    
}

//converts frame from to model from views coordinates
-(CGRect)convertFrameToModel:(CGRect)frame
{
   return CGRectMake(
               frame.origin.x /_surfaceView.frame.size.width,
               frame.origin.y /_surfaceView.frame.size.height,
               frame.size.width/_surfaceView.frame.size.width,
               frame.size.height/_surfaceView.frame.size.height);
}

//converts posDelta from model to views coordinates
-(CGPoint)convertPosDeltaFromModel:(CGPoint)delta
{
    return CGPointMake(delta.x * _surfaceView.frame.size.width, delta.y * _surfaceView.frame.size.height);
}

//converts posDelta to model from views coordinates
-(CGPoint)convertPosDeltaToModel:(CGPoint)delta
{
    return CGPointMake(delta.x / _surfaceView.frame.size.width, delta.y / _surfaceView.frame.size.height);
}

//removes shape from surfaceView.
-(void)removeShape:(ABShape *)shape
{
    ABShapeView *shapeView = _shapesToViews[[NSValue valueWithPointer:(__bridge const void *)shape]];
    [_surfaceView removeShape:shapeView];
    
    [_shapesToViews removeObjectForKey:[NSValue valueWithPointer:(__bridge const void *)shape]];
    [_viewsToShapes removeObjectForKey:[NSValue valueWithPointer:(__bridge const void *)shapeView]];
}


#pragma mark ABSurfaceViewDelegate Methods

//handles pinch event, resizing and updating model
-(void)pinchDetected:(UIPinchGestureRecognizer*)recognizer 
{
	ABShapeView * shapeView = (ABShapeView *)recognizer.view;
	float scale = recognizer.scale;
	recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, scale, scale);
	recognizer.scale = 1;
    
    //update model
	[_model updateShapeFrame:[self convertFrameToModel:shapeView.frame] forShape:[self viewToShape:shapeView]];
}

//handles pan event, also changes the positionDelta if the pan ends, causes acceleration.
-(void)panDetected:(UIPanGestureRecognizer*)recognizer
{
	CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
	
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    [recognizer.view setNeedsDisplay];
	
   
	
	if (recognizer.state == UIGestureRecognizerStateEnded)
    {
		[_model updateShapeFrame:[self convertFrameToModel:recognizer.view.frame ] forShape:[self viewToShape:(ABShapeView*)recognizer.view]];
	}
}

//handles long press event, pulls up colorpicker
-(void)longPressDetected:(UILongPressGestureRecognizer*)recognizer
{
	[self displayColorPickerOnShapeView:(ABShapeView *)recognizer.view];
}

//Displays a colorPicker in a popover on a shapeView, and allows changing of color.
-(void)displayColorPickerOnShapeView:(ABShapeView *)shapeView
{
	//create colorpicker if needed
	if(!_colorPickerPopoverController)
	{
        //set up colorpicker
		_colorPicker = [[ABColorPicker alloc]init];
		_colorPicker.translatesAutoresizingMaskIntoConstraints = NO;
		_colorPicker.userInteractionEnabled = YES;
		
		[_colorPicker addTarget:self action:@selector(colorPickerChanged) forControlEvents:UIControlEventValueChanged];
        //set up view controller and add the picker
		_colorPickerViewController = [[UIViewController alloc]init];
		[_colorPickerViewController.view addSubview:_colorPicker];
		_colorPickerViewController.contentSizeForViewInPopover = CGSizeMake(350.0f,500.0f);
		
        //crete the popover and add the view controller and set constraints
		_colorPickerPopoverController = [[UIPopoverController alloc]initWithContentViewController:_colorPickerViewController];
		
		[_colorPickerPopoverController presentPopoverFromRect:shapeView.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		[_colorPickerPopoverController setDelegate:self];
		
		[_colorPickerViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_colorPicker]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_colorPicker)]];
		
		[_colorPickerViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_colorPicker]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_colorPicker)]];
	}
	
    //save the current shape
	_currentShapeSelectedbyColorPicker = shapeView;
	[_surfaceView bringSubviewToFront:shapeView];
    
	//display the colorpicker
	[_colorPickerPopoverController presentPopoverFromRect:shapeView.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	
	return;
}

//sets the master view controller editing to false when any touch is detected;
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //invalidate editing on the masterViewController
    [_masterViewController setEditing:NO animated:YES];
}

-(void)showColorPickerFromShape:(ABShape*)shape
{
	ABShapeView *shapeView = [self shapeToView:shape];
	[self displayColorPickerOnShapeView:shapeView];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
	//We only want to update the model once the popover dissappears
    [_model updateShapeColor:_colorPicker.color forShape:[self viewToShape:_currentShapeSelectedbyColorPicker]];
	_currentShapeSelectedbyColorPicker = nil;
	return YES;
}

//updates the shapes color when the color picker changes.
-(void)colorPickerChanged
{
    //Don't update the model with every color change,
    //We will only update the model once the popover dissappears
	UIColor *newColor = _colorPicker.color;
	_currentShapeSelectedbyColorPicker.currentColor = newColor;
    [_currentShapeSelectedbyColorPicker setNeedsDisplay];
}

#pragma mark ABSurfaceViewDelegate methods

-(void)updatePositionDelta:(CGPoint)posDelta forShape:(ABShapeView *)shapeView
{
    [_model updateShapePositionDelta:[self convertPosDeltaToModel:posDelta] forShape:[self viewToShape:shapeView]];
}

#pragma mark ABDetailViewController methods
-(ABShape *)viewToShape:(ABShapeView*)shapeView
{
    return _viewsToShapes[[NSValue valueWithPointer:(__bridge const void *)(shapeView)]];
}

-(ABShapeView *)shapeToView:(ABShape*)shape
{
    return _shapesToViews[[NSValue valueWithPointer:(__bridge const void *)(shape)]];
}
							
#pragma mark - UISplitViewControllerDelegate methods

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
	
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
	self.navigationController.navigationBarHidden = NO;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
	self.navigationController.navigationBarHidden = YES;
}

@end
