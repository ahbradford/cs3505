//
//  ABDetailViewController.h
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABShapeModel.h"
#import "ABSurfaceView.h"

@class ABSurfaceView,ABMasterViewController;


@interface ABDetailViewController : UIViewController <UISplitViewControllerDelegate,ABSurfaceViewDelegate,UIPopoverControllerDelegate>


-(void)updateShape:(ABShape *)shape;
-(void)addShape:(ABShape *)shape;
-(void)removeShape:(ABShape *)shape;
-(void)showColorPickerFromShape:(ABShape *)shape;


@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet ABSurfaceView *surfaceView;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property (weak,nonatomic) ABShapeModel* model;
@property (weak,nonatomic) ABMasterViewController * masterViewController;


@end
