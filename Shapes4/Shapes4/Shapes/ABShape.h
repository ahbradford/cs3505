//
//  ABShape.h
//  Shapes
//  A simple shape container class that holds data to represent a shape object
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABShape : NSObject <NSCopying,NSCoding>

@property (nonatomic) UIColor *color;
@property (nonatomic) NSInteger type;
@property (nonatomic) CGRect frame;
@property (nonatomic) CGPoint positionDelta;


-(id)initWithCoder:(NSCoder *)aDecoder;
-(void)encodeWithCoder:(NSCoder *)aCoder;

@end
