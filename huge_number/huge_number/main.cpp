//
//  main.cpp
//  huge_number
//
//  Created by adambradford on 1/11/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;

#include "huge_number.h"


int main(int argc, const char * argv[])
{
#pragma mark Add test;
    huge_number s = huge_number("12345");
    huge_number t =  huge_number("987");
    
    cout << s << endl;
    cout << t << endl;
    cout << endl;
    
    huge_number u = huge_number();
    
    u = s+t;
    
    //cout << u << endl;
    cout << endl;
#pragma mark Multiply test;
    u = s*t;
    
   // cout << u << endl;
    cout << endl;
    
#pragma mark Subtract test
    huge_number sub = s-t;
    huge_number a = huge_number("646846846498741684816868446846868468464668464");
    huge_number b = huge_number("584684684664686846846848385845684646168486654");
    sub = a-b; //should equal 62162161834054837970020061001183822296181810
    cout << sub << " = " << a << " - " << b << endl;
    if(sub.get_value() !="62162161834054837970020061001183822296181810") cout<<"fails -";
    sub = a-a;//should = 0
    cout << sub << " = " << a << " - " << a << endl;
    if(sub.get_value() !="0") cout<<"fails -";
    sub = b-a; //should = 0
    cout << sub << " = " << b << " - " << a << endl;
    if(sub.get_value() !="0") cout<<"fails -";
    
    
//    
//#pragma mark Greater Than Test
//    bool greaterThan = isGreaterThan(s,t);
//    greaterThan = isGreaterThan(t, s);
//    
//    t = "12344";
//    greaterThan = isGreaterThan(s,t);
//    greaterThan = isGreaterThan(t, s);
//    
//    t = "12345";
//    
//    greaterThan = isGreaterThan(s,t);
//    greaterThan = isGreaterThan(t, s);
    
#pragma mark Divide Test
    
    s = huge_number("12345");
    t = huge_number("987");
    huge_number div;
    div = s/t; // should equal 12
    
    cout << div << " = " << s << " / " << t << endl;
    
    if(div.get_value() !="12") cout<<"fails /";
    
    
    a = huge_number("646846846498741684816868446846868468464668464");
    b = huge_number("5846846846646868468468483858");
    div = a/b;// should equal 110631741084462382
    
    cout << div << " = " << a << " / " << b << endl;
    if(div.get_value() !="110631741084462382") cout<<"fails /";
    
    a = huge_number("65656651681811614");
    b = huge_number("3216546546546");
    div = a/b;// should equal 20412
    
    cout << div << " = " << a << " / " << b << endl;
    if(div.get_value() !="20412") cout<<"fails /";
   
   
    
    a = huge_number("656566");
    b = huge_number("32154");
    div = a/b;// should equal 20
    
    cout << div << " = " << a << " / " << b << endl;
    if(div.get_value() !="20") cout<<"fails /";
    
    a = huge_number("6468468464987416848189468146848466868446846868468464668464");
    b = huge_number("584");
    div = a/b;// should equal 11076144631827768575666897511726826829532272035048740870
    cout << div << " = " << a << " / " << b << endl;
    if(div.get_value() !="11076144631827768575666897511726826829532272035048740870") cout<<"fails /";
    
#pragma mark Mod Test
    
    s = huge_number("12345");
    t = huge_number("987");
    huge_number modulus;
    modulus = s%t; // should equal 501
    cout << modulus << " = " << s << " % " << t << endl;
    if(modulus.get_value() !="501") cout<<"fails % 101";
    
    a = huge_number("646846846498741684816868446846868468464668464");
    b = huge_number("5846846846646868468468483858");
    modulus = a%b;// should equal 209521325038391651889438708
    cout << modulus << " = " << a << " % " << b << endl;
    if(modulus.get_value() !="2095213250318391651889438708") cout<<"fails % 106";
    
    a = huge_number("65656651681811614");
    b = huge_number("3216546546546");
    modulus = a%b;// should equal 503573714662
    cout << modulus << " = " << a << " % " << b << endl;
    if(modulus.get_value() !="503573714662") cout<<"fails % 111";
    
    
    a = huge_number("656566");
    b = huge_number("32154");
    modulus = a%b;// should equal 13486
    cout << modulus << " = " << a << " % " << b << endl;
    if(modulus.get_value() !="13486") cout<<"fails % 117";
    
    a = huge_number("6468468464987416848189468146848466868446846868468464668464");
    b = huge_number("584");
    modulus = a%b;// should equal 384
    cout << modulus << " = " << a << " % " << b << endl;
    if(modulus.get_value() !="384") cout<<"fails %122 ";
    
#pragma mark += *= -= ==operators  
    a = huge_number("20");
    b = huge_number("5");
    
    a+= b;
    if(a.get_value() !="25") cout<<"fails +=";
    
    a-=b;
    if(a.get_value() !="20") cout<<"fails -=";
    
    a*=b;
    if(a.get_value() !="100") cout<<"fails *=";
    
#pragma mark Parantheses
    a = huge_number("20");
    b = huge_number("5");

    huge_number c;
    c = a*(a+b);
    
    
    return 0;  // I forgot this in my other examples.
    // Returning '0' signals 'no error' to the shell.
}

