//
//  ABQuarterThrow.m
//  Quarters
//
//  Created by Adam Bradford on 3/12/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABQuarterThrow.h"

@implementation ABQuarterThrow


- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeFloat:_accelorometerX forKey:@"accX"];
    [aCoder encodeFloat:_accelorometerY forKey:@"accY"];
    [aCoder encodeFloat:_accelorometerZ forKey:@"accZ"];
    
    [aCoder encodeFloat:_gyroXAxisRotationRate forKey:@"gX"];
    [aCoder encodeFloat:_gyroYAxisRotationRate forKey:@"gY"];
    [aCoder encodeFloat:_gyroZAxisRotationRate forKey:@"gZ"];
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    ABQuarterThrow *throw = [[ABQuarterThrow alloc]init];
    throw.accelorometerX = [aDecoder decodeFloatForKey:@"accX"];
    throw.accelorometerY = [aDecoder decodeFloatForKey:@"accY"];
    throw.accelorometerZ = [aDecoder decodeFloatForKey:@"accZ"];
    
    throw.gyroXAxisRotationRate = [aDecoder decodeFloatForKey:@"gX"];
    throw.gyroYAxisRotationRate = [aDecoder decodeFloatForKey:@"gY"];
    throw.gyroZAxisRotationRate = [aDecoder decodeFloatForKey:@"gZ"];
    
    return throw;
}

@end
