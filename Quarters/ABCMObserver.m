//
//  ABAccelorometerObserver.m
//  Quarters
//
//  Created by Adam Bradford on 3/12/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABCMObserver.h"
#import "ABQuarterThrow.h"

@interface ABACMObserver()

@property CMMotionManager *motionManager;
@property ABQuarterThrow *previousMotionData;
@end

@implementation ABACMObserver

-(id)init
{
    self = [super init];
    if(self)
    {
        _motionManager = [[CMMotionManager alloc]init];
        
        if(!_previousMotionData)
        {
            _previousMotionData = [[ABQuarterThrow alloc]init];
            _previousMotionData.accelorometerZ = _motionManager.accelerometerData.acceleration.z;
            _triggerTheasholdRate = 2.75;
        }
        
    }
    
    return  self;
}

-(void)monitorMotion
{
    _motionManager.accelerometerUpdateInterval = .05;
    NSOperationQueue *motionQueue = [[NSOperationQueue alloc]init];
    [_motionManager startAccelerometerUpdatesToQueue:motionQueue withHandler:
                ^(CMAccelerometerData *accelerometerData, NSError *error)
                {
                  
                    if(fabsf(_previousMotionData.accelorometerZ - _motionManager.accelerometerData.acceleration.z >_triggerTheasholdRate))
                    {
                        ABQuarterThrow *throw = [[ABQuarterThrow alloc]init];
                        throw.accelorometerX = _motionManager.accelerometerData.acceleration.x;
                        throw.accelorometerY = _motionManager.accelerometerData.acceleration.y;
                        throw.accelorometerZ = _motionManager.accelerometerData.acceleration.z;
                        
                        [_delegate quarterWasThrown:throw];
                        
                        NSLog(@"X: %.2f Y: %.2f Z: %.2f",throw.accelorometerX,throw.accelorometerY,throw.accelorometerZ);                       
                    }
                    
                }];
}

@end
