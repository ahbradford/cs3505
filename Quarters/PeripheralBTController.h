//
//  PeripheralBTController.h
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PeripheralBTControllerDelegate <NSObject>
-(void)dataReceivedFromCentral:(NSData*)data;


@end

@interface PeripheralBTController : NSObject
-(void)stopAdvertising;
-(void)sendDatatoCentral:(NSData *)data;
@property id<PeripheralBTControllerDelegate> delegate;


@end
