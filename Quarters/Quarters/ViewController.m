//
//  ViewController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ViewController.h"
#import "CentralBTController.h"
#import "PeripheralBTController.h"
#import "ABCMObserver.h"
#import "ABQuarterThrow.h"

@interface ViewController () <CentralBTControllerDelegate,PeripheralBTControllerDelegate>
@property (strong,nonatomic) CentralBTController       *central;
@property (strong,nonatomic) PeripheralBTController    *peripheral;
@property (strong,nonatomic) ABACMObserver             *motionObserver;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _peripheral = [[PeripheralBTController alloc]init];
    _peripheral.delegate = self;
    _motionObserver = [[ABACMObserver alloc]init];
    _motionObserver.delegate = self;
    [_motionObserver monitorMotion];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dataReceivedfromPeripherial:(NSData *)data
{
    ABQuarterThrow *throw = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    self.textField.text = [NSString stringWithFormat:@"X: %.2f Y: %.2f Z: %.2f",throw.accelorometerX,throw.accelorometerY,throw.accelorometerZ] ;
}
-(void)dataReceivedFromCentral:(NSData *)data
{
    
    self.textField.text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
}

- (IBAction)switched:(UISwitch *)sender
{
    if(sender.on == NO)
    {
        _central = [[CentralBTController alloc]init];
        _central.delegate = self;
        [_peripheral stopAdvertising];
    
        _peripheral = nil;
    }
    else
    {
        _central = nil;
        _peripheral = [[PeripheralBTController alloc]init];
        _peripheral.delegate = self;
    }
}
- (IBAction)send
{
    if(_peripheral)
    {
        [_peripheral sendDatatoCentral:[_textField.text dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if(_central)
    {
        [_central sendDatatoPeripherial:nil data:[_textField.text dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

#pragma mark ABCMObserverDelegate methods

-(void)quarterWasThrown:(ABQuarterThrow *)throw
{
    [_peripheral sendDatatoCentral:[NSKeyedArchiver archivedDataWithRootObject:throw]];
    NSLog(@"Throw sent to central");
}
@end
