/*
 * This is a tester similar to the tester written in class.  It reads
 * words from a text file, then adds the words to two sets: A built-in
 * set class, and our utah_set class.  After reading the file, it
 * prints out all the words stored in the STL set object.  At the end
 * of the test, it prints out the sizes of both sets to see that they
 * are the same.
 *
 * After the test completes, I make sure the local variabls are properly
 * cleaned up.
 *
 * If the comments wrap lines, widen your emacs window.
 *
 * Peter Jensen
 * January 17, 2013
 *
 *
 * Modified by Adam Bradford u0616767
 * 21 Jan 2013
 */

#include <iostream>
#include <fstream>
#include <set>
#include <iterator>
#include "utah_set.h"
#include "node.h"

using namespace std;



int main ()
{
    bool testPassed = true;
  // Open up another block.  This way, when the block ends,
  // variables local to the block will be destroyed, but main
  // will still be running.  (Did you know that you can open
  // up a block at any time to control local variable scope and
  // lifetime?)
  
  {
    // Create the two sets.  Declaring the local variables constructs the objects.
  
    set<string>      stl_set_of_words;  // The built-in set class - no constructor parameters.

    cs3505::utah_set our_set_of_words(1000);  // Our set class, with a hashtable of 1000 slots.
  
    // Open the file stream for reading.  (We'll be able to use it just like
    //   the keyboard stream 'cin'.)

    ifstream in("Yankee.txt");

    // Loop for reading the file.  Note that it is controlled
    //   from within the loop (see the 'break').
    
    while (true)
    {
      // Read a word (don't worry about punctuation)
      
      string word;
      in >> word;

      // If the read failed, we're probably at end of file
      //   (or else the disk went bad).  Exit the loop.
      
      if (in.fail())
	break;

      // Word successfully read.  Add it to both sets.
      
      stl_set_of_words.insert(word);
      our_set_of_words.add(word);
    }

    // Close the file.

    in.close();

    

  }
    
  
  /////Test add/////
    
    {
        cout << "Testing add function:" << endl;
        //set up like before
        set<string>      stl_set_of_words;  // The built-in set class - no constructor parameters.
        cs3505::utah_set our_set_of_words(1000);  // Our set class, with a hashtable of 1000 slots
        ifstream in("Yankee.txt");
        
        while (true)
        {
            string word;
            in >> word;
            if (in.fail())
                break;
            stl_set_of_words.insert(word);
            our_set_of_words.add(word);
        }
        
        
        //add two of the same nonsense words to sets, ensure count only goes up by one
        int stlcount = (int)stl_set_of_words.size();
        stl_set_of_words.insert("bling");
        stl_set_of_words.insert("bling");
        our_set_of_words.add("bling");
        our_set_of_words.add("bling");
        if(our_set_of_words.size() != stlcount +1)
        {
            cout<<"Failed add test (your set contains duplicates)"<<endl;
            testPassed = false;
        }
        
        
        //test for contains and add
        if(!our_set_of_words.contains("bling"))
        {
            cout<<"Failed add test (your set did not add the word 'bling')"<<endl;
            testPassed = false;
        }
        
        
        //test for remove
        cout << "Testing remove function:" << endl;
        
        our_set_of_words.remove("bling");
        if(our_set_of_words.contains("bling"))
        {
            cout<<"Failed remove test (your set did not remove the word 'bling')"<<endl;
            testPassed = false;
        }
        if(our_set_of_words.size() != stlcount)
        {
            cout<<"Failed remove test (your set did not decrement counter on remove)"<<endl;
            testPassed = false;
        }
    
        //remove word "Commanded" which linked to bling (bling will be inserted at the end of the linked list
        //add 'bling' and then 'Commanded' to test removing from the middle of the linked list.
        our_set_of_words.remove("commanded");
        our_set_of_words.add("bling");
        our_set_of_words.add("commanded");
        
        //redo the test
        our_set_of_words.remove("bling");
        if(our_set_of_words.contains("bling"))
        {
            cout<<"Failed remove test (your set did not remove the word 'bling')"<<endl;
            testPassed = false;
        }
        if(our_set_of_words.size() != stlcount)
        {
            cout<<"Failed remove test (your set did not decrement counter on remove)"<<endl;
            testPassed = false;
        }
    
        //test removing an element twice
        {
            our_set_of_words.remove("bling");
        }
        
        cout << "Testing get elements function:" << endl;
        //get_elements test
        string *stringArray = new string[our_set_of_words.size()];
        
        
        
        our_set_of_words.get_elements(stringArray, our_set_of_words.size());
        
        //iterate through array and ensure each element is contained in our set
        int elementCount = 0;
        for(int i = 0; i < our_set_of_words.size(); i ++)
        {
            if(!our_set_of_words.contains(stringArray[i])&& stringArray[i].size() > 0)
            {
                cout << "Failed get_elements test (expected element not found in set)"<<endl;
                testPassed = false;
                break;
            }
            elementCount++;
        }
        if(elementCount != our_set_of_words.size())
        {
            cout << "Failed get_elements test (incorrect number expected " << our_set_of_words.size() << " But returned " << elementCount << endl;
            testPassed = false;
        }
        
        //new we try it with a number smaller than the array and ensure the that the element after the last in the array is not in the set, because we would be out of bound.
        
        
        stringArray = new string[501];
        

        our_set_of_words.get_elements(stringArray, 500);
        
        //iterate through array and ensure each element is contained in our set
        elementCount = 0;
        for(int i = 0; i < 500; i ++)
        {
            if(!our_set_of_words.contains(stringArray[i])&& stringArray[i].size() > 0)
            {
                cout << "Failed get_elements test (expected element not found in set)"<<endl;
                testPassed = false;
                break;
            }
            elementCount++;
        }
        
        //ensure the 500th array element is in the set
        if(!our_set_of_words.contains(stringArray[499]))
        {
            cout << "Failed get_elements test (an expected element was not found"<<endl;
            testPassed = false;
        }
        //ensure the 501th array element is not contained in the set.
        if(our_set_of_words.contains(stringArray[500]))
        {
            cout << "Failed get_elements test (an unexpecetd element was found while requesting a get_elements with less than the set size"<<endl;
            testPassed = false;
        }
    }

    ///////Test = operator
    {
        cout << "Testing equals function:" << endl;
        //Make utah_set and one stl set and fill;
        set<string>      stl_set_of_words;  // The built-in set class - no constructor parameters.
        cs3505::utah_set our_set_of_words(1000);  // Our set class, with a hashtable of 1000 slots
        
        ifstream in("Yankee.txt");
        
        while (true)
        {
            string word;
            in >> word;
            if (in.fail())
                break;
            stl_set_of_words.insert(word);
            our_set_of_words.add(word);
        }

        
        //make a new set
        cs3505::utah_set set2(500);
        
        //set it equal to our previous set
        set2 = our_set_of_words;
        
        //get set contents
        string* array = new string[our_set_of_words.size()];
        our_set_of_words.get_elements(array,our_set_of_words.size());
        
        //check new set has all works from old set
        for(int i = 0; i < our_set_of_words.size(); i ++)
        {
            if(!set2.contains(array[i]))
            {
                cout << "Fails equal test, did not copy word: " << array[i] << endl;
                testPassed = false;
            }
        }
    }

    cout << "Testing constructor and desctructor functions:" << endl;
    //tests utah_set constructor calls and destructor calls, they should be equal
    if(cs3505::utah_set::constructor_count() != cs3505::utah_set::destructor_count())
    {
        cout << "Fails utah_set Destructor test: Constructor count does not equal Destructor count" << endl;

    }
 
    //tests node constructor calls and destructor calls, they should be equal
    if(cs3505::node::constructor_count() != cs3505::node::destructor_count())
    {
        cout << "Fails node Destructor test: Constructor count does not equal Destructor count" << endl;
    }

    
    cout << endl;
    //list if we passed...
    if(testPassed)
    {
      cout << "Passed all tests!" << endl;
    }
    else
    {
        cout << "Failed 1 or more tests, see above for more information" <<endl;
    }
  
    return 0;
}


