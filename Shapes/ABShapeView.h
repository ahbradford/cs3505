//
//  ABShapeView.h
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABShapeView;
@protocol ABShapeViewDelegate <NSObject>
-(void)pinchDetected:(UIPinchGestureRecognizer*)recognizer;
-(void)panDetected:(UIPanGestureRecognizer*)recognizer;
-(void)longPressDetected:(UILongPressGestureRecognizer*)recognizer;
@end

@interface ABShapeView : UIView <NSCopying,UIGestureRecognizerDelegate>


@property (nonatomic,readonly) NSInteger type;
@property (weak,nonatomic) UIColor* currentColor;
@property (weak,nonatomic) id<ABShapeViewDelegate> delegate;
@property CGPoint positionDelta;

-(id)initWithFrame:(CGRect)frame type:(NSInteger)type;
//renders and returns UIImage of the current view.
-(UIImage *)imageByRenderingView;


@end
