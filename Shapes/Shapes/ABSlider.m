//
//  ABSlider.m
//  ColorPicker
//
//  Created by adambradford on 2/3/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABSlider.h"


@interface ABSlider()

@property double knobHeight;

@end

@implementation ABSlider
@synthesize value = _value;
@synthesize knobHeight = _knobHeight;

-(void)setValue:(float)value
{
  _value = fmaxf(0.0f, fminf(255, value));
  
  
}
-(id)init
{
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      _value = 128;
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  [[UIColor blackColor]set];
  CGContextFillRect(context, rect);
  
  
  
  
  CGContextMoveToPoint(context, 0+_knobHeight/2, self.bounds.size.height/2);
  CGContextAddLineToPoint(context, self.bounds.size.width-_knobHeight/2, self.bounds.size.height/2);
  [[UIColor grayColor]setStroke];
  CGContextDrawPath(context, kCGPathStroke);
  _knobHeight = self.bounds.size.height/2;
  CGRect knobLocation = CGRectMake( (_value/255)*(self.bounds.size.width - _knobHeight) , _knobHeight/2,_knobHeight,_knobHeight);
  CGContextAddEllipseInRect(context, knobLocation);
  [[UIColor darkGrayColor] setFill];
  [[UIColor grayColor]setStroke];
  CGContextDrawPath(context, kCGPathFillStroke);
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
  UITouch *touch = [touches anyObject];
  CGPoint location = [touch locationInView:self];
  self.value = ((location.x+_knobHeight)/(self.bounds.size.width)*255) ;
  
  [self sendActionsForControlEvents:UIControlEventValueChanged];
  [self setNeedsDisplay];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  
  [self sendActionsForControlEvents:UIControlEventEditingDidEnd];

}



@end
