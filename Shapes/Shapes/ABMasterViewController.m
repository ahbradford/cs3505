//
//  ABMasterViewController.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//


#import "ABMasterViewController.h"

#import "ABDetailViewController.h"
#import "ABShape.h"

@interface ABMasterViewController ()
{
    NSMutableArray *_objects;
    
}
@property bool viewNeedsUpdate;
@property NSMutableDictionary *shapePointers;
@property NSTimer *timer;
@end

@implementation ABMasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.title = NSLocalizedString(@"Shape List", @"Shape List");
		_shapeTableView.sectionIndexColor = [UIColor whiteColor];
		self.
		
		self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
		self.navigationItem.rightBarButtonItem = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
	self.navigationItem.rightBarButtonItem = addButton;
    
    //initalize instance variables
    _shapes = [[_model getAllShapes]mutableCopy];
    _shapePointers = [[NSMutableDictionary alloc]init];
    _deleteAllButton.hidden = YES;
	_shapeTableView.allowsSelection = YES;
    
    
    //creates timer.  Adjust the interval to smaller for more frequent updates, but lower performace when displaying lots of shapes
    _timer = [NSTimer scheduledTimerWithTimeInterval:.2f target:self selector:@selector(updateTableView) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Random shape creation methods
//action for insert random object button.
- (IBAction)insertRandomObject:(id)sender
{
    ABShape *shape = [[ABShape alloc]init];
    shape.frame = [self randomFrame];
    shape.positionDelta = [self randomPositionDelta];
    shape.type = arc4random() % 9 +2;
    shape.color = [self randomColor];
    [_model addShape:shape];
}

//this method is called when the + button is pressed
- (void)insertNewObject:(id)sender
{
    [self insertRandomObject:nil];	
}

//returns a new random UIColor
-(UIColor *)randomColor
{
    float r = ((float)(arc4random() %255))/255;
    float g = ((float)(arc4random() %255))/255;
    float b = ((float)(arc4random() %255))/255;
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}

//returns a random position delta < 1;
-(CGPoint)randomPositionDelta
{
    float x = ((float)(arc4random() %100))/10000;
    float y = ((float)(arc4random() %100))/10000;
    if(arc4random()%2 == 0) x = -x;
    if(arc4random()%2 == 0) y = -y;
    return CGPointMake(x, y);
}

//returns a new frame with random x and y <1 position and equal sides < 1
-(CGRect)randomFrame
{
    //create a random H/W 1/4th of the width of the W/H of the surfaceView area
    float width= (float)(((arc4random()% (int)_detailViewController.surfaceView.frame.size.width/4)))/_detailViewController.surfaceView.frame.size.width ;
    float height= (float)(((arc4random()% (int)_detailViewController.surfaceView.frame.size.height/4)))/_detailViewController.surfaceView.frame.size.height;
    
    //make sure the numbers arent too small.
    if(width < .1)width = .2;
    if(height < .1)height = .1;
    
    //create the origins.
    float xPos = ((arc4random() % 100) /200.0) +.2;
    float yPos = ((arc4random() % 100) /200.0) +.2;
    
    if(xPos < .1)xPos = .2;
    if(yPos < .1)yPos = .2;
    
    if(xPos > .9)xPos = .8;
    if(yPos > .9)yPos = .8;
    
    return CGRectMake(xPos, yPos, width, height);
    
}

//reloads table data
-(void)updateTableView
{
    [_shapeTableView reloadData];
}

#pragma mark ABMasterViewController mutator methods

//updates shape
-(void)updateShape:(ABShape *)shape
{
    _viewNeedsUpdate = true;
}

//Adds shape to the table
-(void)addShape:(ABShape *)shape
{
    [_shapes addObject:shape];
    NSNumber *shapeAddress = [NSNumber numberWithUnsignedInt:(uint)shape];
    [_shapePointers setObject:shape forKey:shapeAddress];
	if(_shapeTableView.editing ==YES)
	{
		[self setEditing:NO animated:YES];
		[_shapeTableView reloadData];
		[self setEditing:YES animated:YES];
	}
}

//removes a shape from table.
-(void)removeShape:(ABShape *)shape
{
    if([_shapes containsObject:shape]) [_shapes removeObject:shape];
}

//returns the name for the number of sides
-(NSString *)nameForSides:(NSInteger)sides
{
    switch (sides) {
        case 2:
            return @"Circle";
        case 3:
            return @"Triangle";
        case 4:
            return @"Rectangle";
        case 5:
            return @"Pentagon";
        case 6:
            return @"Hexagon";
        case 7:
            return @"Heptagon";
        case 8:
            return @"Octagon";
        case 9:
            return @"Nonagon";
        case 10:
            return @"Decagon";
            
        default:
            return @"";
            break;
    }
}

//Deletes all shapes
- (IBAction)deleteAllPressed:(id)sender
{
    NSArray *allShapes = [_shapes copy];
    for(ABShape *s in allShapes)
    {
        [_model removeShape:s];
    }
    [_shapeTableView reloadData];
    [self setEditing:NO animated:YES];
}

- (IBAction)infoButtonPressed
{
    NSString *infoString = @"-Press \"Insert Random Shape\" or the + Button to insert a new shape\n\n-Drag a shape to change its position and velocity.\n\n-Pinch a shape to change it's size.\n\n-Select shape in table, or long press on a shape to change its color.\n\n-Press the \"Edit\" button to delete individual shapes or delete all shapes.";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Shapes Info"
                                                    message:infoString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

//called when editing button is pressed.
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    
	if(!_shapeTableView.editing)
	{
		[_shapeTableView setEditing:editing animated:animated];
	}
	else
	{
		[_shapeTableView setEditing:NO animated:YES];
		editing = NO;
	}
	
    if(editing == YES)
    {
        //stop updating table
        [_timer invalidate];
        _timer = nil;
		self.editButtonItem.title = @"Done";
		self.editButtonItem.tintColor = [UIColor orangeColor];
        _deleteAllButton.hidden = NO;
        
    }
    else
    {
        //start updating table again.
        //make sure its not running first
        if(!_timer)
        {
            _timer = [NSTimer scheduledTimerWithTimeInterval:.2f target:self selector:@selector(updateTableView) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            [_timer fire];
        }
		self.editButtonItem.title = @"Edit";
		self.editButtonItem.tintColor = nil;
        _deleteAllButton.hidden = YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _shapes.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    //set text of cell
	ABShape *shape = _shapes[indexPath.row];
	cell.textLabel.text = [self nameForSides:shape.type];
    cell.textLabel.textColor = shape.color;
	float centerX = (shape.frame.origin.x + shape.frame.size.width/2)*_detailViewController.surfaceView.frame.size.width;
	float centerY = (shape.frame.origin.y + shape.frame.size.height/2)*_detailViewController.surfaceView.frame.size.height;
	float width = shape.frame.size.width * _detailViewController.surfaceView.frame.size.width;
	float height = shape.frame.size.height * _detailViewController.surfaceView.frame.size.height;
	
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Center:%.0f,%.0f W:%.0f H:%.0f",centerX,centerY,width,height];
    
    //checks if the cell is resued and needs to draw a new icon.
    if([_shapePointers objectForKey:[NSNumber numberWithUnsignedInt:(uint)shape]] != [NSNumber numberWithUnsignedInt:(uint)shape])
    {
        [_shapePointers removeObjectForKey:[NSNumber numberWithUnsignedInt:(uint)shape]];
        [_shapePointers setObject:shape forKey:[NSNumber numberWithUnsignedInt:(uint)shape]];;
        
		ABShapeView *temp = [[ABShapeView alloc]initWithFrame:CGRectMake(0, 0, 30, 30) type:shape.type];
        temp.currentColor = shape.color;
        cell.imageView.image = [temp imageByRenderingView];
    }
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [_model removeShape:_shapes[indexPath.row]];
        [tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	//DetailViewController shows colorPicker when row is selected.
    [_detailViewController showColorPickerFromShape:_shapes[indexPath.row]];
}


@end
