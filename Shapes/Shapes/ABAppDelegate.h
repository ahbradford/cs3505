//
//  ABAppDelegate.h
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABDetailViewController.h"
#import "ABMasterViewController.h"
#import "ABShapeModel.h"

@interface ABAppDelegate : UIResponder <UIApplicationDelegate,ABShapeModelDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@end
