//
//  ABColorCrossHair.h
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABColorCrossHair : UIView

@property (nonatomic) CGPoint currentLocation;

@end
