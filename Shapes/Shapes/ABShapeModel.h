//
//  ABShapeModel.h
//  Shapes
//  Creates a model to hold the state of a shape objects.
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ABShape;

//ABShapeModel protocol
@protocol ABShapeModelDelegate <NSObject>
-(void)shapeDidChange:(ABShape*)shape;
-(void)shapeAdded:(ABShape *)shape;
-(void)shapeRemoved:(ABShape *)shape;
@end

@interface ABShapeModel : NSObject<NSCoding>

//methods for accessing and updating
-(NSArray*)getAllShapes;
-(void)updateShapePositionDelta:(CGPoint)posDelta forShape:(ABShape *)shape;
-(void)updateShapeFrame:(CGRect)frame forShape:(ABShape *)shape;
-(void)updateShapeColor:(UIColor*)color forShape:(ABShape *)shape;
-(void)removeShape:(ABShape *)shape;
-(void)addShape:(ABShape *)shape;
-(void)updateModelLogic;

//archiving methods.
-(void)encodeWithCoder:(NSCoder *)aCoder;
-(id)initWithCoder:(NSCoder *)aDecoder;

@property id<ABShapeModelDelegate>delegate;

@end
