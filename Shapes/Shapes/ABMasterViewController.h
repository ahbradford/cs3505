//
//  ABMasterViewController.h
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABShapeModel.h"
#import "ABDetailViewController.h"

@class ABDetailViewController;

@interface ABMasterViewController : UIViewController <UITableViewDelegate>

-(void)updateShape:(ABShape *)shape;
-(void)addShape:(ABShape *)shape;
-(void)removeShape:(ABShape *)shape;

- (IBAction)deleteAllPressed:(id)sender;
- (IBAction)infoButtonPressed;


@property (strong, nonatomic) ABDetailViewController *detailViewController;
@property (weak, nonatomic) IBOutlet UITableView *shapeTableView;
@property (nonatomic) NSMutableArray *shapes;
@property (strong) ABShapeModel* model;
@property (weak, nonatomic) IBOutlet UIButton *deleteAllButton;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;


@end
