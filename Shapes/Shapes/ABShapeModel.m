//
//  ABShapeModel.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABShapeModel.h"
#import "ABShape.h"


@interface ABShapeModel()

@property (nonatomic) NSMutableArray *shapesArray;

@end


@implementation ABShapeModel

@synthesize delegate = _delegate;


-(id)init
{
    self = [super init];
    if(self)
    {
    //Cant really see using more than 30 objects...
    _shapesArray = [[NSMutableArray alloc]initWithCapacity:30];
        
        
    }
    return self;    
}


#pragma mark update model Methods

//updates position delta for a given shape
-(void)updateShapePositionDelta:(CGPoint)posDelta forShape:(ABShape *)shape;
{
    shape.positionDelta = posDelta;
    //the update model logic will send this message to the delegate.
    //[_delegate shapeDidChange:shape];
    
}

//update shapeFrame for a given shape
-(void)updateShapeFrame:(CGRect)frame forShape:(ABShape *)shape;
{
    shape.frame = frame;
    [_delegate shapeDidChange:shape];
}

//update shapeColor for a given shape
-(void)updateShapeColor:(UIColor *)color forShape:(ABShape *)shape;
{
    shape.color = color;;
    [_delegate shapeDidChange:shape];
}

//adds a shape to the model and notifies delegate
-(void)addShape:(ABShape *)shape
{
    [_shapesArray addObject:shape];
    [_delegate shapeAdded:shape];
}

//removes a shape from the model and notifies the delegate.
-(void)removeShape:(ABShape *)shape
{
    [_shapesArray removeObject:shape];
    [_delegate shapeRemoved:shape];
}

//fire this at a rate of .015s, or ELSE!!!!
-(void)updateModelLogic
{
    
    for(ABShape* s in _shapesArray)
    {
        //calculate new frame using position delta
        CGRect newFrame;
        newFrame.origin.x = s.frame.origin.x + s.positionDelta.x;
        newFrame.origin.y = s.frame.origin.y + s.positionDelta.y;
        newFrame.size = s.frame.size;
        s.frame = newFrame;
        
        //normalize position delta to slow things down to a normal rate
        CGPoint point = s.positionDelta;
        if(point.x > .001) point.x = point.x - .0001;
        if(point.y > .001) point.y = point.y - .0001f;
        if(point.x < -.001) point.x = point.x + .0001f;
        if(point.y < -.001) point.y = point.y + .0001f;
       
        s.positionDelta = point;
        
        
        //notifiy delegate
        [_delegate shapeDidChange:s];
      
        
    }
}

//returns array of all ABshapes
-(NSArray *)getAllShapes
{
    NSMutableArray *newArray = [NSMutableArray array];
    for(ABShape *s in _shapesArray)
    {
        [newArray addObject:[s copy]];
    }
    return newArray;
}


#pragma mark NSCoder protocol methods
-(id)initWithCoder:(NSCoder *)decoder
{
	if ((self=[super init])) {
        _shapesArray = [decoder decodeObjectForKey:@"shapesArray"];
		
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:_shapesArray forKey:@"shapesArray"];
	
}

@end


