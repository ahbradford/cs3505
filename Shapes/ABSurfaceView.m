//
//  ABSurfaceView.m
//  Shapes
//
//  Created by adambradford on 2/20/13.
//  Copyright (c) 2013 adambradford. All rights reserved.
//

#import "ABSurfaceView.h"
#import "ABShapeView.h"

@implementation ABSurfaceView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _shapeSet = [[NSMutableSet alloc]init];
	}
    return self;
}

-(void)awakeFromNib
{	
	self.backgroundColor = [UIColor blueColor];
    _shapeSet = [[NSMutableSet alloc]init];
}

//add shape to view
-(void)addShape:(ABShapeView *)shape
{
    [self addSubview:shape];
    shape.delegate = self;
    [_shapeSet addObject:shape];
}

//remove shape from view
-(void)removeShape:(ABShapeView *)shape
{
    [_shapeSet removeObject:shape];
    [shape removeFromSuperview];
}

#pragma mark ABShapeViewDelegate methods
-(void)pinchDetected:(UIPinchGestureRecognizer*)recognizer
{
	[_delegate pinchDetected:recognizer];
}

-(void)panDetected:(UIPanGestureRecognizer*)recognizer
{
	[_delegate panDetected:recognizer];
}

-(void)longPressDetected:(UILongPressGestureRecognizer*)recognizer
{
	[_delegate longPressDetected:recognizer];
}

//checks for collisions and updates the positionDelta if a shape has gone past the surface view bounds.
-(void)checkForCollisions
{
	for (ABShapeView *shape in _shapeSet)
	{
		if(!CGRectContainsRect(self.bounds, shape.frame))
		{   CGPoint point;
			point.x = shape.positionDelta.x;
			point.y = shape.positionDelta.y;
            
			if(shape.frame.origin.x <= 0)point.x = abs(point.x);
			if(shape.frame.origin.y <= 0)point.y = abs(point.y);
			if(shape.frame.origin.x + shape.frame.size.width >= self.frame.size.width)point.x = - fabs(point.x);
			if(shape.frame.origin.y + shape.frame.size.height >= self.frame.size.height) point.y = -fabs(point.y);
			
			if(point.x == 0 && shape.frame.origin.x <= self.frame.origin.x) point.x = 1.0f;
			if(point.y == 0 && shape.frame.origin.y <= self.frame.origin.y) point.y = 1.0f;
			if(point.x == 0 && shape.frame.origin.x + shape.frame.size.width >= self.frame.size.width) point.x = 1.0f;
			if(point.y == 0 && shape.frame.origin.y + shape.frame.size.height >= self.frame.size.height) point.y = 1.0f;
			
            [_delegate updatePositionDelta:point forShape:shape];	
		}
	}
}

@end
